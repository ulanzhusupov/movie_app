import 'package:flutter/material.dart';
import 'package:movie_app/presentation/theme/app_fonts.dart';

class DescriptionWidget extends StatelessWidget {
  const DescriptionWidget({
    super.key,
    required this.description,
  });

  final String description;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width * 0.85,
      child: Text(
        description,
        style: AppFonts.s11W300.copyWith(
          color: Colors.white,
        ),
      ),
    );
  }
}

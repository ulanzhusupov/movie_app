import 'package:flutter/material.dart';

abstract class AppFonts {
  static TextStyle s13W400 =
      const TextStyle(fontSize: 13, fontWeight: FontWeight.w400);
  static TextStyle s13W500 =
      const TextStyle(fontSize: 13, fontWeight: FontWeight.w500);
  static TextStyle s18W500 =
      const TextStyle(fontSize: 18, fontWeight: FontWeight.w500);
  static TextStyle s11W300 = const TextStyle(
      fontSize: 11, fontWeight: FontWeight.w300, color: Colors.white);
  static TextStyle s12W500 = const TextStyle(
      fontSize: 12, fontWeight: FontWeight.w500, color: Colors.white);
  static TextStyle s10W500 = const TextStyle(
      fontSize: 10, fontWeight: FontWeight.w500, color: Colors.white);
}

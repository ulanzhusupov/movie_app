import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:movie_app/core/network/dio_settings.dart';
import 'package:movie_app/data/models/poster_model.dart';
import 'package:movie_app/presentation/theme/app_fonts.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PosterScreen extends StatefulWidget {
  const PosterScreen({super.key});

  @override
  State<PosterScreen> createState() => _PosterScreenState();
}

class _PosterScreenState extends State<PosterScreen> {
  PosterModel model = PosterModel();
  TextEditingController commentController = TextEditingController();
  TextEditingController searchController = TextEditingController();
  SharedPreferences? prefs;

  Future<void> getPoster() async {
    final Dio dio = DioSettings().dio;
    try {
      final Response response =
          await dio.get("http://www.omdbapi.com/?i=tt3896198&apikey=ab78215");
      model = PosterModel.fromJson(response.data);
      setState(() {});
    } catch (e) {
      print(e);
    }
  }

  Future<void> getPosterByName(String name) async {
    final Dio dio = DioSettings().dio;
    try {
      final Response response =
          await dio.get("http://www.omdbapi.com/?t=$name&apikey=ab78215");
      model = PosterModel.fromJson(response.data);
      setState(() {});
    } catch (e) {
      print(e);
    }
  }

  @override
  void initState() async {
    // TODO: implement initState
    super.initState();
    getPoster();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: [
            Container(
              color: Colors.black,
              child: Padding(
                padding: const EdgeInsets.only(
                  left: 20,
                  right: 20,
                  top: 40,
                  bottom: 14,
                ),
                child: SizedBox(
                  height: 48,
                  child: TextField(
                    controller: searchController,
                    style: AppFonts.s13W500.copyWith(
                      color: const Color(0xffB1B1B1),
                    ),
                    decoration: InputDecoration(
                      contentPadding:
                          const EdgeInsets.only(left: 15, bottom: 10),
                      label: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          const Icon(
                            Icons.search_outlined,
                            size: 15,
                            color: Color(0xffB1B1B1),
                          ),
                          const SizedBox(width: 12),
                          Text(
                            "Search Malls or Branch",
                            style: AppFonts.s13W500.copyWith(
                              color: const Color(0xffB1B1B1),
                            ),
                          ),
                        ],
                      ),
                      suffix: Padding(
                        padding: const EdgeInsets.only(right: 10),
                        child: IconButton(
                          onPressed: () {
                            getPosterByName(searchController.text);
                          },
                          icon: const Icon(Icons.search),
                          color: Colors.white,
                        ),
                      ),
                      filled: true,
                      fillColor: const Color(0xff5A5A5C),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(25.0),
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Stack(
              children: [
                Image(
                  image: NetworkImage(
                    model.poster ?? "",
                  ),
                  width: double.infinity,
                  fit: BoxFit.cover,
                ),
                Container(
                  height: MediaQuery.of(context).size.height * 1,
                  decoration: const BoxDecoration(
                    gradient: LinearGradient(
                      colors: [
                        Color(0xff1E1F27),
                        Colors.transparent,
                      ],
                      begin: Alignment.center,
                      end: Alignment.topCenter,
                    ),
                  ),
                ),
                Positioned(
                  top: MediaQuery.of(context).size.height * 0.5,
                  child: Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 30),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(5),
                            color: const Color(0xff484747),
                          ),
                          child: Padding(
                            padding: const EdgeInsets.all(4.0),
                            child: Text(model.genre.toString(),
                                style: AppFonts.s13W500
                                    .copyWith(color: Colors.white)),
                          ),
                        ),
                        const SizedBox(height: 28),
                        Text(
                          "${model.title.toString()} ${model.year}",
                          style: AppFonts.s18W500.copyWith(
                            color: Colors.white,
                          ),
                        ),
                        const SizedBox(height: 10),
                        SizedBox(
                          width: MediaQuery.of(context).size.width * 0.85,
                          child: Text(
                            model.plot ?? "empty",
                            style: AppFonts.s11W300.copyWith(
                              color: Colors.white,
                            ),
                          ),
                        ),
                        const SizedBox(height: 30),
                        SizedBox(
                          height: 300,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Text(
                                "10.4K Comments",
                                style: AppFonts.s12W500,
                              ),
                              const SizedBox(height: 20),
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.end,
                                children: [
                                  const CircleAvatar(
                                    backgroundColor: Colors.lightBlue,
                                    radius: 17.5,
                                  ),
                                  const SizedBox(width: 13),
                                  SizedBox(
                                    width: 281,
                                    child: TextField(
                                      style: AppFonts.s11W300,
                                      controller: commentController,
                                      decoration: InputDecoration(
                                        suffixIcon: IconButton(
                                          padding:
                                              const EdgeInsets.only(top: 5),
                                          onPressed: () async {
                                            prefs = await SharedPreferences
                                                .getInstance();
                                            await prefs?.setString("comment",
                                                commentController.text);
                                            setState(() {});
                                          },
                                          icon: const Icon(
                                            Icons.telegram,
                                            color: Colors.white,
                                            size: 30,
                                          ),
                                        ),
                                        hintText: "Add a comment...",
                                        hintStyle: AppFonts.s11W300.copyWith(
                                            color: const Color(0xff8F8F8F)),
                                        border: const UnderlineInputBorder(
                                          borderSide: BorderSide(
                                            width: 1,
                                            color: Color(0xff646464),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                              const SizedBox(height: 46),
                              Row(
                                children: [
                                  const CircleAvatar(
                                    backgroundColor: Colors.lightBlue,
                                    radius: 17.5,
                                  ),
                                  const SizedBox(width: 13),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        "Andrew Garfield",
                                        style: AppFonts.s10W500,
                                      ),
                                      Text(
                                        prefs?.getString('comment') ??
                                            "This trailer looks sick! So excited to see this! <3",
                                        style: AppFonts.s11W300
                                            .copyWith(color: Colors.white),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
